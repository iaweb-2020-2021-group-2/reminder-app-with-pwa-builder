import { LitElement, css, html, customElement, property } from 'lit-element';


@customElement('app-footer')
export class AppHeader extends LitElement {

  @property({ type: String }) title: string = 'PWA Starter';

  static get styles() {
    return css`
      .nav__container {
          position: absolute;
          bottom: 0;
          width: 100%;
          display: flex;
          justify-content: space-around;
      }

      .nav__button {
          width: 50%;
          border-radius: 0;
          padding: 8px 0;
      }

      .nav__button:hover {
          background: rgba(255, 255, 255, 0.25);
      }

      @media only screen and (min-width: 768px) {
          .nav__container {
              bottom: 100%;
              top: 0;
          }
      }
    `;
  }

  constructor() {
    super();
  }

  render() {
    return html`
        <nav class="nav__container">
          <fast-anchor class="nav__button" href="./" appearance="button">Home</fast-anchor>
          <fast-anchor class="nav__button" href="./about" appearance="button">About</fast-anchor>
        </nav>
    `;
  }
}