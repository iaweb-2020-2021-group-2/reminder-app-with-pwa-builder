import { LitElement, css, html, customElement, property } from 'lit-element';
import '@vaadin/vaadin-text-field';
import '@vaadin/vaadin-button';
import '@vaadin/vaadin-checkbox';
import '@vaadin/vaadin-date-time-picker';
// For more info on the @pwabuilder/pwainstall component click here https://github.com/pwa-builder/pwa-install
import '@pwabuilder/pwainstall';
import 'app-datepicker';

class Task {
  public task: string;
  public date: string;
  public completed: boolean;

  constructor(task: string, date: string, completed: boolean) {
    this.task = task;
    this.date = date;
    this.completed = completed;
  }
}

@customElement('app-home')
export class AppHome extends LitElement {

  // For more information on using properties in lit-element
  // check out this link https://lit-element.polymer-project.org/guide/properties#declare-with-decorators
  @property() todos: Array<Task> = [];
  @property({type: String}) task = "";
  @property({type: String}) date = "";

  static get styles() {
    return css`
      .todo__container {
        margin: auto;
        max-width: 700pt;
        display: flex;
        flex-direction: column;
      }

      .todo__formElement {
        width: 100%;
        margin: 0 auto 10px;
        color: white;
      }

      .todo__formButton {
        width: 100%;
        padding: 8px;
        border: 1px solid #333;
        background: #3489eb;
        color: white;
        font-weight: normal;
      }

      .todo__button--small {
        width: fit-content;
        padding: 8px;
        border: 1px solid #333;
        background: #3489eb;
        color: white;
        font-weight: normal;
      }

      .todo__addIcon {
        width: 4%;
      }

      .todo__separation {
        width: 100%;
        border-color: rgba(255, 255, 255, 0.25);
      }

      .todo__item {
        display: flex;
        border-radius: 5px;
        margin: 0 auto 10px;
        padding: 8px 16px;
        background: #252525;
        justify-content: space-between;
      }

      .todo__itemTask {

      }

      .todo__itemDate {
        width: 50%;
      }

      .todo__datePicker {
        width: 100%;
        color: white;

      }

      .todo__itemCheckbox {
        float: right;
      }
    `;
  }

  constructor() {
    super();
    //init todos from local storage
    this.todos = [];
    this.loadFromStorage();
  }

  share() {
    if ((navigator as any).share) {
      (navigator as any).share({
        title: 'PWABuilder pwa-starter',
        text: 'Check out the PWABuilder pwa-starter!',
        url: 'https://github.com/pwa-builder/pwa-starter',
      })
    }
  }

  onSubmit() {
    console.log("onsubmit");
  }


  render() {
    return html`
      <div class="todo__container"
        onload="${this.askNotificationPermission()}"
      >
        <div id="welcomeBar" @keyup="${this.shortcutListener}" >
      
            <vaadin-text-field
              class="todo__formElement"
              placeholder="Task"
              value="${this.task}"
              @change="${this.updateTask}">
            </vaadin-text-field>
            <vaadin-date-time-picker theme="custom-overlay-style" class="todo__datePicker" @change="${this.updateDate}"></vaadin-date-time-picker>
            <vaadin-button class="todo__formButton"
              @click="${this.addTodo}">
              <img class="todo__addIcon" src="assets/icons/plus_icon.png"/> TODO
            </vaadin-button>
        </div>

        <hr class="todo__separation">

        <div class="todos-list">
          ${this.listTodos()}
        </div>

        <vaadin-button class="todo__button--small" @click="${this.registerNotification}">Notification Demo</vaadin-button>
      </div>
      
      <pwa-install>Install PWA Starter</pwa-install>
      
    `;
  }

  listTodos(){
    if(!this.todos){
      return "";
    }

    return this.todos.map(todo => html`
      <div class="todo__item">
      <div class="todo__itemTask">Task: ${todo.task}</div>
      <div class="todo__itemDate">
        Date: 
        ${new Date(todo.date).getDate()}.${new Date(todo.date).getMonth()+1}.${new Date(todo.date).getFullYear()}</div> 
        <vaadin-checkbox class="todo__itemCheckbox" ?checked="${todo.completed}" @change="${(e: { target: { checked: boolean; }; }) => this.updateTodoStatus(todo, e.target.checked)}">
        </vaadin-checkbox>
      </div>
    `);
  }

  loadFromStorage(){
    this.todos = JSON.parse(localStorage.getItem("todo") || '[]');
  }

  async registerNotification() {
    var reg = await navigator.serviceWorker.getRegistration();
    Notification.requestPermission().then(() => {
      reg?.showNotification('Demo Title', {
        body: 'This is a demo notification.',
        timestamp: new Date().getUTCDate()
      });
    });
  }

  askNotificationPermission() {
    Notification.requestPermission(function(status) {
      console.log('Notification permission status:', status);
    });
  }

  updateTodoStatus(updatedTodo: Task, complete: boolean){
    if(!this.todos){
      return
    }

    this.todos = this.todos.map(todo =>
      updatedTodo === todo ? {...updatedTodo, complete} : todo
      );
  }

  updateTask(e: any){
    this.task = e.target.value;
  }

  updateDate(e: any){
    this.date = e.target.value;
  }

  isValidDate(d: Date) {
    return d instanceof Date;
  }

  addTodo(){
    if(this.task.trim() != "" && this.date.trim() != "") {
      this.todos.push(
        new Task(this.task, this.date, false)
      );

      console.log("Added task");

      this.task = "";
      this.date = "";
    }
    this.updateStorage();
  }

  updateStorage() {
    localStorage.setItem("todo", JSON.stringify(this.todos));
  }

  shortcutListener(e: any) {
    if(e.key === 'Enter'){
      this.addTodo();
    }
  }
}